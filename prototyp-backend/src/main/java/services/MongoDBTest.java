package services;

import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

public class MongoDBTest {
    private MongoClient client;
    private DB prototyp;
    private DBCollection collection;

    public void init() throws UnknownHostException {
        client = new MongoClient(new MongoClientURI("mongodb://193.196.54.12:27021"));
        prototyp = client.getDB("PrototypDB");
        collection = prototyp.getCollection("Person");
    }

    public void addPeople() {
        List<Integer> books = Arrays.asList(27464, 747854);
        DBObject person = new BasicDBObject("_id", "9")
                .append("name", "Dominik")
                .append("address", new BasicDBObject("street", "Musterstraße")
                        .append("city", "Musterstadt")
                        .append("PLZ", 11111))
                .append("books", books);
        DBObject person2 = new BasicDBObject("_id", "4")
                .append("name", "Alfred")
                .append("address", new BasicDBObject("street", "Schlossallee")
                        .append("city", "Monopoly-Stadt")
                        .append("PLZ", 66666))
                .append("books", books);
        collection.insert(person);
        collection.insert(person2);
    }

    public void close() {
        client.close();
    }

    public void ask() {
        DBObject query = new BasicDBObject("_id", "1");
        DBCursor cursor = collection.find(query);
        System.out.println((String) cursor.one().get("name"));
        cursor.close();
    }

    public void askWithMoreResults() {
        DBObject query = new BasicDBObject("name", "Alfred");
        DBCursor cursor = collection.find(query);
        while (cursor.hasNext()) {
            System.out.println(cursor.next());
        }
        cursor.close();
    }
}
