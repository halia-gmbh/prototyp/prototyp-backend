package backend.services;

import backend.entity.Account;
import backend.entity.DBConnector;
import backend.entity.MongoClientManager;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class AccountService {

    private Map<String, Account> accounts = new HashMap<>();

    /**
     * Registriert einen neuen Account
     *
     * @param username Username
     * @param password Passwort
     * @return UserID oder -1, falls Username vergeben
     */
    public int registrate(String username, String password) {
        if (accounts.containsKey(username)) {
            return -1;
        }
        int id = accounts.size();
        Account account = new Account(username, password, id);
        accounts.put(username, account);
        saveInDB();
        return id;
    }

    /**
     * Einloggen
     *
     * @param username Username
     * @param password Passwort
     * @return UserID oder -1, falls Eingaben falsch
     */
    public int login(String username, String password) {
        if (accounts.containsKey(username)) {
            Account account = accounts.get(username);
            if (account.getPassword().equals(password)) {
                return account.getId();
            }
        }
        return -1;
    }


    /**
     * Creates the AccountService responsible for account management and login
     */
    public AccountService() {
        try (MongoClientManager c = DBConnector.getConnection()) {
            DBCollection x = c.getClient().getDB("Prototyp").getCollection("Account");
            DBCursor cur = x.find();
            while (cur.hasNext()) {
                Account account = new Account(cur.next());
                accounts.put(account.getName(), account);
            }
            cur.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves all current Accounts into the database.
     */
    public void saveInDB() {
        try (MongoClientManager c = DBConnector.getConnection()) {
            DBCollection x = c.getClient().getDB("Prototyp").getCollection("Account");
            x.drop();
            for (Account acc : accounts.values()) {
                x.insert(acc.toDBObject());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
