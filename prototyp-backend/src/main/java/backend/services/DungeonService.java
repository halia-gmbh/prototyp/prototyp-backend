package backend.services;

import backend.entity.Game;
import backend.entity.charaktergestaltung.CharacterOptions;
import backend.entity.charaktergestaltung.Equipment;
import backend.entity.charaktergestaltung.PlayerClass;
import backend.entity.charaktergestaltung.Race;
import backend.entity.raumgestaltung.NPC;
import backend.entity.raumgestaltung.Room;
import backend.entity.spieler.Character;
import backend.entity.spieler.Player;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DungeonService {

    private final Map<Integer, Game> allDungeons = new HashMap<>();
    private final Map<Integer, Game> activeDungeons = new HashMap<>();

    /**
     * Creates new Dungeon
     *
     * @param name Name of the dungeon
     * @return DungeonID
     */
    public int createDungeon(String name) {
        Game game = new Game(name, allDungeons.size());
        allDungeons.put(game.getId(), game);
        return game.getId();
    }

    /**
     * Adds a new Room to a Dungeon
     *
     * @param dungeonID DungeonID
     * @param room      newRoom
     */
    public Room addRoomToDungeon(int dungeonID, Room room) {
        if (!allDungeons.containsKey(dungeonID)) {
            return null;
        }
        return allDungeons.get(dungeonID).addRoom(room);

    }

    /**
     * Connects 2 rooms
     *
     * @param dungeonID DungeonID
     * @param roomID1   Id of room1
     * @param roomID2   Id of room2
     */
    public boolean connectRooms(int dungeonID, int roomID1, int roomID2) {
        if (!allDungeons.containsKey(dungeonID)) {
            return false;
        }
        return allDungeons.get(dungeonID).connectRooms(roomID1, roomID2);
    }

    /**
     * Adds a new race to a dungeon
     *
     * @param dungeonID DugeonID
     * @param race      new race
     */
    public boolean addRaceToDungeon(int dungeonID, Race race) {
        if (!allDungeons.containsKey(dungeonID)) {
            return false;
        }
        return allDungeons.get(dungeonID).addRace(race);
    }

    /**
     * Adds a new class to a dungeon
     *
     * @param dungeonID      DugeonID
     * @param newPlayerClass new class
     */
    public boolean addClassToDungeon(int dungeonID, PlayerClass newPlayerClass) {
        if (!allDungeons.containsKey(dungeonID)) {
            return false;
        }
        return allDungeons.get(dungeonID).addClass(newPlayerClass);
    }

    /**
     * Adds new Equipment to a dungeon
     *
     * @param dungeonID DugeonID
     * @param equipment new Equipment
     */
    public boolean addEquipmentToDungeon(int dungeonID, Equipment equipment) {
        if (!allDungeons.containsKey(dungeonID)) {
            return false;
        }
        return allDungeons.get(dungeonID).addEquipment(equipment);
    }

    /**
     * Returns all Dungeons
     *
     * @return Map of DungeonID and Dungeonname
     */
    public Map<Integer, String> getAllDungeons() {
        Map<Integer, String> map = new HashMap<>();
        for (Game s : allDungeons.values()) {
            if (!s.isActive()) {
                map.put(s.getId(), s.getName());
            }
        }
        return map;
    }

    /**
     * Returns all actice Dungeons
     *
     * @return Map of DungeonID and Dungeonname
     */
    public Map<Integer, String> getActiveDungeons() {
        Map<Integer, String> map = new HashMap<>();
        for (Game s : activeDungeons.values()) {
            map.put(s.getId(), s.getName());
        }
        return map;
    }

    /**
     * Checks, if a Account already has a Player in a Dungeon
     *
     * @param dungeonID DungeonID
     * @param userID    User-ID
     * @return True, if yes
     */
    public boolean checkExistingPlayers(int dungeonID, int userID) {
        return activeDungeons.get(dungeonID).checkExistingPlayer(userID);
    }

    /**
     * Returns Options for a new Character
     *
     * @param dungeonID DungeonID
     * @return Options
     */
    public CharacterOptions getCharakterOptions(int dungeonID) {
        return activeDungeons.get(dungeonID).getCharacterOptions();
    }

    /**
     * Adds a new Player to a Dungeon
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     * @param name      Name of the Player
     * @param character selected character options
     * @return True, if creation was successful
     */
    public boolean addPlayerToDungeon(int dungeonID, int userID, String name, Character character) {
        return activeDungeons.get(dungeonID).createPlayer(userID, name, character);
    }

    /**
     * Deletes a player from a dungeon
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     * @return True, if deletion was successful
     */
    public boolean deletePlayerFromDungeon(int dungeonID, int userID) {
        return activeDungeons.get(dungeonID).deletePlayer(userID);
    }

    /**
     * Lets a  Player join a dungeon and places him in the start room
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     * @return True, if successful
     */
    public boolean joinDungeon(int dungeonID, int userID) {
        return activeDungeons.get(dungeonID).join(userID);
    }

    /**
     * Lets a player exit a dungeon
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     * @return True, if successful
     */
    public boolean exitDungeon(int dungeonID, int userID) {
        return activeDungeons.get(dungeonID).exit(userID);
    }

    /**
     * Activates an inactive dungeon.
     *
     * @param dungeonID DungeonID
     * @param userID    UserID of the DungeonMaster
     * @return True, if successful
     */
    public boolean activateDungeon(int dungeonID, int userID) {
        if (!activeDungeons.containsKey(dungeonID)) {
            allDungeons.get(dungeonID).startGame(userID);
            activeDungeons.put(dungeonID, allDungeons.get(dungeonID));
            return true;
        }
        return false;
    }

    /**
     * Deactivates a dungeon. Only possible, if no player is inside.
     *
     * @param dungeonID DungeonID
     * @param userID    UserID of DungeonMaster
     * @return True, if successful
     */
    public boolean deactivateDungeon(int dungeonID, int userID) {
        if (activeDungeons.get(dungeonID).endGame(userID)) {
            activeDungeons.remove(dungeonID);
            return true;
        }
        return false;
    }

    /**
     * Places player in another room of a dungeon.
     * Only possible, if the new room is connected to his currrent room.
     *
     * @param dungeonID DungonID
     * @param userID    UserID
     * @param room      new Room
     * @return True, if successful
     */
    public boolean switchRoom(int dungeonID, int userID, int room) {
        return activeDungeons.get(dungeonID).switchRoom(userID, room);
    }

    /**
     * Executes the interaction of a player with an oject.
     * The player picks up equipment and replaces his current one.
     *
     * @param dungeonID     DungeonID
     * @param userID        UserID
     * @param equipmentname Name of the equipment he wants to pick up.
     * @return True, if successful
     */
    public boolean interactWithObject(int dungeonID, int userID, String equipmentname) {
        return activeDungeons.get(dungeonID).interactWithObject(userID, equipmentname);
    }

    /**
     * Returns the player of a user
     *
     * @param dungeonID Dungeon
     * @param userID    UserID
     * @return The player
     */
    public Player getCharakter(int dungeonID, int userID) {
        return allDungeons.get(dungeonID).getCharakter(userID);
    }

    /**
     * Returns a dungeon
     *
     * @param dungeonID DungeonID
     * @return the Dungeon
     */
    public Game getDungeon(int dungeonID) {
        return allDungeons.get(dungeonID);
    }

    /**
     * Returns a room
     *
     * @param dungeonID DungeonID
     * @param room      RoomID
     * @return the room
     */
    public Room getRoom(int dungeonID, int room) {
        return allDungeons.get(dungeonID).getRoom(room);
    }

    /**
     * Returns the UserID of the Dungeon-Master of a Dungeon.
     *
     * @param dungeonID DungeonID
     * @return the UserID
     */
    public int getDungeonMaster(int dungeonID) {
        return activeDungeons.get(dungeonID).getDungeonMaster();
    }

    /**
     * Adds an object to a room
     *
     * @param dungeonID DungeonID
     * @param room      RoomID
     * @param equipment the object
     * @return True, if successful
     */
    public boolean addObjectToRoom(int dungeonID, int room, Equipment equipment) {
        return activeDungeons.get(dungeonID).addObjectToRoom(room, equipment);
    }

    /**
     * Deletes an object from a room
     *
     * @param dungeonID DungeonID
     * @param room      RoomID
     * @param name      Name of the object
     * @return True, if successful
     */
    public boolean deleteObjectFromRoom(int dungeonID, int room, String name) {
        return activeDungeons.get(dungeonID).deleteObject(room, name);
    }

    /**
     * Adds a NPC to a  room
     *
     * @param dungeonID DungeonID
     * @param room      RoomID
     * @param npc       the NPC
     * @return True, if successful
     */
    public boolean addNPC(int dungeonID, int room, NPC npc) {
        return activeDungeons.get(dungeonID).addNPC(room, npc);
    }

    /**
     * Deletes a NPC from a room
     *
     * @param dungeonID DungeonID
     * @param room      RoomID
     * @param name      Name of the NPC
     * @return True, if successful
     */
    public boolean deleteNPC(int dungeonID, int room, String name) {
        return activeDungeons.get(dungeonID).deleteNPC(room, name);
    }

    /**
     * Updates a Player-Object in dungeon
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     * @param player    the new Player-Object
     * @return True, if successful
     */
    public boolean playerUpdate(int dungeonID, int userID, Player player) {
        return activeDungeons.get(dungeonID).playerUpdate(userID, player);
    }

    /**
     * Returns all Players in a Room as a list of their IDs. Searches by a UserID
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     * @return True, if successful
     */
    public List<Integer> getPlayersAsList(int dungeonID, int userID) {
        return activeDungeons.get(dungeonID).getPlayersAsList(userID);
    }

    /**
     * Returns all Players in a Room as a list of their IDs. Searches by a RoomID
     *
     * @param dungeonID DungeonID
     * @param room      RoomID
     * @return True, if successful
     */
    public List<Integer> getPlayersInRoom(int dungeonID, int room) {
        return activeDungeons.get(dungeonID).getPlayersInRoom(room);
    }
}
