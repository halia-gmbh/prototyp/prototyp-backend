package backend.entity.raumgestaltung;

/**
 * NPC-Information
 */
public class NPC {

    private String name;
    private int healthPoints;
    private int attackDamage;

    public String getName() {
        return name;
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    public int getAttackDamage() {
        return attackDamage;
    }
}
