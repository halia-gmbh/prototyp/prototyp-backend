package backend.entity.raumgestaltung;


import backend.entity.charaktergestaltung.Equipment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Dungeon. Contains all rooms
 */
public final class Dungeon {

    private final Map<Integer, Room> roomList = new HashMap<>();
    private final Map<Integer, Integer> playerRoomRelation = new HashMap<>();
    private Room startRoom;

    /**
     * Adds a startroom
     *
     * @param room room
     * @return roomID (0)
     */
    public Room addStartraum(Room room) {
        room.setId(0);
        roomList.put(0, room);
        startRoom = room;
        return room;
    }

    /**
     * Adds a room
     *
     * @param room room
     * @return roomID
     */
    public Room addRoom(Room room) {
        int id = roomList.size();
        room.setId(id);
        roomList.put(id, room);
        return room;
    }

    /**
     * Connects 2 rooms
     *
     * @param raumID1 ID of room1
     * @param raumID2 ID of room2
     * @return True, if successful
     */
    public boolean connectRooms(int raumID1, int raumID2) {
        if (roomList.containsKey(raumID1) && roomList.containsKey(raumID2)) {
            roomList.get(raumID1).connectRoom(raumID2);
            roomList.get(raumID2).connectRoom(raumID1);
            return true;
        }
        return false;
    }

    /**
     * Adds a player to the dungeon
     *
     * @param userID     UserID
     * @param playername name
     * @return True, if successful
     */
    public boolean addPlayer(int userID, String playername) {
        playerRoomRelation.put(userID, 0);
        return startRoom.addPlayer(userID, playername);
    }

    /**
     * Removes a player from the dungeon
     *
     * @param userID userID
     * @return True, if successful
     */
    public boolean removePlayer(int userID) {
        int raumID = playerRoomRelation.get(userID);
        playerRoomRelation.remove(userID);
        roomList.get(raumID).removePlayer(userID);
        return true;
    }

    /**
     * Puts player into connected room
     *
     * @param userID userID
     * @param raumID new room
     * @return True, if successful
     */
    public boolean switchRooms(int userID, int raumID) {
        int aktuellerRaum = playerRoomRelation.get(userID);
        if (roomList.get(aktuellerRaum).isConnectedTo(raumID)) {
            String playername = roomList.get(aktuellerRaum).removePlayer(userID);
            roomList.get(raumID).addPlayer(userID, playername);
            playerRoomRelation.replace(userID, raumID);
            return true;
        }
        return false;
    }

    /**
     * Interaction between player and object
     *
     * @param userID        userID
     * @param equipmentname object-name
     * @param equipment     current Equipment of the player
     * @return object
     */
    public Equipment interactWithObject(int userID, String equipmentname, Equipment equipment) {
        int raumID = playerRoomRelation.get(userID);
        return roomList.get(raumID).interactWithObject(equipmentname, equipment);
    }

    /**
     * returns room count
     *
     * @return room count
     */
    public int getRoomCount() {
        return roomList.size();
    }

    /**
     * returns room
     *
     * @param userID userID
     * @return room
     */
    public Room getRoom(int userID) {
        return roomList.get(playerRoomRelation.get(userID));
    }

    /**
     * Adds an object to a room
     *
     * @param raum      room
     * @param equipment object
     * @return True, if successful
     */
    public boolean addObjectToRoom(int raum, Equipment equipment) {
        return roomList.get(raum).addObject(equipment);
    }

    /**
     * Deletes an object from a room
     *
     * @param raum room
     * @param name object-name
     * @return True, if successful
     */
    public boolean deleteObjectFromRoom(int raum, String name) {
        return roomList.get(raum).deleteObject(name);
    }

    /**
     * Adds a NPC to a room
     *
     * @param raum room
     * @param npc  npc
     * @return True, if successful
     */
    public boolean addNPC(int raum, NPC npc) {
        return roomList.get(raum).addNPC(npc);
    }

    /**
     * Removes a NPC from a room
     *
     * @param raum room
     * @param name npc-name
     * @return True, if successful
     */
    public boolean deleteNPC(int raum, String name) {
        return roomList.get(raum).deleteNPC(name);
    }

    /**
     * Returns users in a room as a list
     *
     * @param userID userID
     * @return UserIDs in list
     */
    public List<Integer> getPlayersAsList(int userID) {
        int raumID = playerRoomRelation.get(userID);
        return roomList.get(raumID).getPlayersAsList();
    }

    /**
     * Returns users in a room as a list
     *
     * @param raumID room
     * @return UserIDs in list
     */
    public List<Integer> getPlayersInRoom(int raumID) {
        return roomList.get(raumID).getPlayersAsList();
    }

    public Map<Integer, Room> getRoomList() {
        return roomList;
    }

    public Map<Integer, Integer> getPlayerRoomRelation() {
        return playerRoomRelation;
    }


}
