package backend.entity.raumgestaltung;

import backend.entity.charaktergestaltung.Equipment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Room in a dungeon
 */
public class Room {

    private int id;
    private String name;
    private final List<NPC> npcList = new ArrayList<>();
    private final List<Equipment> objectList = new ArrayList<>();

    private final List<Integer> connectedRooms = new ArrayList<>();
    private final Map<Integer, String> players = new HashMap<>();

    /**
     * Connects this room to another one
     *
     * @param raumID ID of the other room
     */
    public void connectRoom(int raumID) {
        if (!connectedRooms.contains(raumID)) {
            connectedRooms.add(raumID);
        }
    }

    /**
     * Adds a player
     *
     * @param userID     userID
     * @param playerName name
     * @return True, if successful
     */
    public boolean addPlayer(int userID, String playerName) {
        players.put(userID, playerName);
        return true;
    }

    /**
     * Removes a player
     *
     * @param userID userID
     * @return True, if successful
     */
    public String removePlayer(int userID) {
        return players.remove(userID);
    }

    /**
     * Player picks up Equipment and drops his current.
     *
     * @param equipmentName name of new Equipment
     * @param equipment     Current Equipment
     * @return new Equipment
     */
    public Equipment interactWithObject(String equipmentName, Equipment equipment) {
        for (Equipment e : objectList) {
            if (e.getName().equals(equipmentName)) {
                objectList.add(equipment);
                objectList.remove(e);
                return e;
            }
        }
        return null;
    }

    /**
     * Checks connection between rooms
     *
     * @param raumID other room
     * @return True, if connected
     */
    public boolean isConnectedTo(int raumID) {
        return connectedRooms.contains(raumID);
    }

    /**
     * Adds an object
     *
     * @param equipment object
     * @return True, if successful
     */
    public boolean addObject(Equipment equipment) {
        return objectList.add(equipment);
    }

    /**
     * Deletes an object
     *
     * @param name object-name
     * @return True, if successful
     */
    public boolean deleteObject(String name) {
        for (Equipment e : objectList) {
            if (e.getName().equals(name)) {
                return objectList.remove(e);
            }
        }
        return false;
    }

    /**
     * Adds a NPC
     *
     * @param npc npc
     * @return True, if successful
     */
    public boolean addNPC(NPC npc) {
        return npcList.add(npc);
    }

    /**
     * Deletes a npc
     *
     * @param name name of the npc
     * @return True, if successful
     */
    public boolean deleteNPC(String name) {
        for (NPC npc : npcList) {
            if (npc.getName().equals(name)) {
                return npcList.remove(npc);
            }
        }
        return false;
    }

    /**
     * Returns UserIDs
     *
     * @return List of UserIDs
     */
    public List<Integer> getPlayersAsList() {
        return new ArrayList<>(players.keySet());
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<NPC> getNpcList() {
        return npcList;
    }

    public List<Equipment> getObjectList() {
        return objectList;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Map<Integer, String> getPlayers() {
        return players;
    }


}
