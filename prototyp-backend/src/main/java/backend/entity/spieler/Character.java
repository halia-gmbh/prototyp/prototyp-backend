package backend.entity.spieler;

import backend.entity.charaktergestaltung.Equipment;
import backend.entity.charaktergestaltung.PlayerClass;
import backend.entity.charaktergestaltung.Race;

/**
 * Character-Information of a player
 */
public class Character {

    private final Race race;
    private final PlayerClass playerClass;
    private Equipment equipment;

    private int healthPoints;
    private int maxHealthPoints;
    private int attackDamage;

    public Character(Race race, PlayerClass playerClass, Equipment equipment) {
        this.race = race;
        this.playerClass = playerClass;
        this.equipment = equipment;
        getHP();
    }

    /**
     * Calculates the Character-stats
     */
    public void getHP() {
        maxHealthPoints = race.getHealthPoints() + playerClass.getHealthPoints() + equipment.getHealthPoints();
        healthPoints = maxHealthPoints;
        attackDamage = race.getAttackDamage() + playerClass.getAttackDamage() + equipment.getAttackDamage();
    }

    public Race getRace() {
        return race;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public PlayerClass getPlayerClass() {
        return playerClass;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }
}
