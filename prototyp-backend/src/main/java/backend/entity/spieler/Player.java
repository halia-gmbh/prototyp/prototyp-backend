package backend.entity.spieler;

import backend.entity.charaktergestaltung.Equipment;

/**
 * Player in a dungeon
 */
public class Player {

    private final String name;

    private final Character character;

    public Player(String name, Character character) {
        this.name = name;
        this.character = character;
    }

    public String getName() {
        return name;
    }

    public Character getCharacter() {
        return character;
    }

    public void setEquipment(Equipment equipment) {
        character.setEquipment(equipment);
    }
}
