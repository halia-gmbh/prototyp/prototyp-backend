package backend.entity.charaktergestaltung;

import java.util.ArrayList;
import java.util.List;

/**
 * Daten zu Gestaltungsoptionen für einen Charakter in einem Dungeon
 */
public class CharacterOptions {

    private final List<Race> races = new ArrayList<>();
    private final List<PlayerClass> playerClasses = new ArrayList<>();
    private final List<Equipment> equipments = new ArrayList<>();

    /**
     * Adds new race
     *
     * @param race new race
     */
    public boolean addRace(Race race) {
        if (!races.contains(race)) {
            races.add(race);
            return true;
        }
        return false;
    }

    /**
     * adds new class
     *
     * @param aPlayerClass new class
     */
    public boolean addClass(PlayerClass aPlayerClass) {
        if (!playerClasses.contains(aPlayerClass)) {
            playerClasses.add(aPlayerClass);
            return true;
        }
        return false;
    }

    /**
     * adds new equipment
     */
    public boolean addEquipment(Equipment equipment) {
        if (!equipments.contains(equipment)) {
            equipments.add(equipment);
            return true;
        }
        return false;
    }

    /**
     * Checks if race is contained
     *
     * @param race race
     * @return True, if yes
     */
    public boolean containsRace(Race race) {
        for (Race r : races) {
            if (race.getName().equals(r.getName()) || race.getAttackDamage() == r.getAttackDamage() || race.getHealthPoints() == r.getHealthPoints()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if class is contained
     *
     * @param aPlayerClass class
     * @return True, if yes
     */
    public boolean containsClass(PlayerClass aPlayerClass) {
        for (PlayerClass r : playerClasses) {
            if (aPlayerClass.getName().equals(r.getName()) || aPlayerClass.getAttackDamage() == r.getAttackDamage() || aPlayerClass.getHealthPoints() == r.getHealthPoints()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if equipment is contained
     *
     * @param equipment equipment
     * @return True, if yes
     */
    public boolean containsEquipment(Equipment equipment) {
        for (Equipment r : equipments) {
            if (equipment.getName().equals(r.getName()) || equipment.getAttackDamage() == r.getAttackDamage() || equipment.getHealthPoints() == r.getHealthPoints()) {
                return true;
            }
        }
        return false;
    }

    public List<Equipment> getEquipments() {
        return equipments;
    }

    public List<PlayerClass> getPlayerClasses() {
        return playerClasses;
    }

    public List<Race> getRaces() {
        return races;
    }
}
