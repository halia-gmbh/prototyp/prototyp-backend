package backend.entity.charaktergestaltung;

/**
 * Equipment-data
 */
public class Equipment {

    private final String name;
    private final int healthPoints;
    private final int attackDamage;

    public Equipment(String name, int healthPoints, int attackDamage) {
        this.name = name;
        this.attackDamage = attackDamage;
        this.healthPoints = healthPoints;
    }

    public int getAttackDamage() {
        return attackDamage;
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    public String getName() {
        return name;
    }
}
