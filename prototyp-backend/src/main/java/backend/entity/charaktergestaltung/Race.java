package backend.entity.charaktergestaltung;

/**
 * Race-data
 */
public class Race {

    private final String name;
    private final int healthPoints;
    private final int attackDamage;

    public Race(String name, int healthPoints, int attackDamage) {
        this.name = name;
        this.attackDamage = attackDamage;
        this.healthPoints = healthPoints;
    }

    public int getAttackDamage() {
        return attackDamage;
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    public String getName() {
        return name;
    }
}
