package backend.entity;

import backend.entity.charaktergestaltung.CharacterOptions;
import backend.entity.charaktergestaltung.Equipment;
import backend.entity.charaktergestaltung.PlayerClass;
import backend.entity.charaktergestaltung.Race;
import backend.entity.raumgestaltung.Dungeon;
import backend.entity.raumgestaltung.NPC;
import backend.entity.raumgestaltung.Room;
import backend.entity.spieler.Character;
import backend.entity.spieler.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Main-Class for a Game
 */
public class Game {

    //allgemein
    private final String name;
    private final int id;
    public boolean active = false;

    //Aufbau
    private final Dungeon dungeon;
    private final CharacterOptions characterOptions = new CharacterOptions();

    //Spieler
    private final List<Integer> activePlayers = new ArrayList<>();
    private final Map<Integer, Player> playerIdRelation = new HashMap<>();
    private int dungeonMaster = -1;

    public Game(String name, int id) {
        this.name = name;
        this.id = id;
        dungeon = new Dungeon();
    }

    /**
     * Starts the game
     *
     * @param userID UserID of DungenMaster
     */
    public void startGame(int userID) {
        active = true;
        dungeonMaster = userID;
    }

    /**
     * Closes the game
     *
     * @param userID UserID of DungeonMaster
     * @return True, if successful
     */
    public boolean endGame(int userID) {
        if (dungeonMaster == userID) {
            active = false;
            dungeonMaster = -1;
            return true;
        }
        return false;
    }

    /**
     * Creates a Player
     *
     * @param userID    Account-ID
     * @param name      Playername
     * @param character Character
     * @return True, if successful
     */
    public boolean createPlayer(int userID, String name, Character character) {
        if (characterOptions.containsEquipment(character.getEquipment()) &&
                characterOptions.containsClass(character.getPlayerClass()) &&
                characterOptions.containsRace(character.getRace())) {
            Player player = new Player(name, character);
            playerIdRelation.put(userID, player);
            return true;
        }
        return false;
    }

    /**
     * Deletes Player
     *
     * @param userID Account-ID
     */
    public boolean deletePlayer(int userID) {
        if (activePlayers.contains(userID)) {
            exit(userID);
        }
        playerIdRelation.remove(userID);
        return true;
    }


    /**
     * Joins Player into dungeon
     *
     * @param userID UserID
     * @return True, if successful
     */
    public boolean join(int userID) {
        if (playerIdRelation.containsKey(userID)) {
            activePlayers.add(userID);
            return dungeon.addPlayer(userID, playerIdRelation.get(userID).getName());
        }
        return false;
    }

    /**
     * Removes Player from Dungeon
     *
     * @param userID UserID
     * @return True, if successful
     */
    public boolean exit(int userID) {
        activePlayers.remove(Integer.valueOf(userID));
        return dungeon.removePlayer(userID);
    }

    /**
     * Adds new race
     *
     * @param race new race
     */
    public boolean addRace(Race race) {
        return characterOptions.addRace(race);
    }

    /**
     * Adds new playerClass
     *
     * @param aPlayerClass new class
     */
    public boolean addClass(PlayerClass aPlayerClass) {
        return characterOptions.addClass(aPlayerClass);
    }

    /**
     * Adds new equipment
     *
     * @param equipment new Equipment
     */
    public boolean addEquipment(Equipment equipment) {
        return characterOptions.addEquipment(equipment);
    }

    /**
     * Adds new room
     *
     * @param room new room
     * @return True, if successful
     */
    public Room addRoom(Room room) {
        if (dungeon.getRoomCount() == 0) {
            return dungeon.addStartraum(room);
        } else {
            return dungeon.addRoom(room);
        }
    }

    /**
     * Connects 2 rooms
     *
     * @param raumID1 Id of room1
     * @param raumID2 Id of room2
     * @return True, if successful
     */
    public boolean connectRooms(int raumID1, int raumID2) {
        return dungeon.connectRooms(raumID1, raumID2);
    }

    /**
     * Checks, if a User already has a player
     *
     * @param userID User-ID
     * @return True, if yes
     */
    public boolean checkExistingPlayer(int userID) {
        return playerIdRelation.containsKey(userID);
    }


    /**
     * Puts player into connected room
     *
     * @param userID UserID
     * @param raumID new Room
     * @return True, if successful
     */
    public boolean switchRoom(int userID, int raumID) {
        if (activePlayers.contains(userID)) {
            return dungeon.switchRooms(userID, raumID);
        }
        return false;
    }

    /**
     * Interaction between Player and Object
     *
     * @param userID        UserID
     * @param equipmentname Object-name
     * @return True, if successful
     */
    public boolean interactWithObject(int userID, String equipmentname) {
        Equipment aktuellesEquipment = playerIdRelation.get(userID).getCharacter().getEquipment();
        Equipment neuesEquipment = dungeon.interactWithObject(userID, equipmentname, aktuellesEquipment);
        if (neuesEquipment != null) {
            playerIdRelation.get(userID).setEquipment(neuesEquipment);
            return true;
        }
        return false;
    }

    /**
     * @return characteroptions
     */
    public CharacterOptions getCharacterOptions() {
        return characterOptions;
    }

    /**
     * Returns room
     *
     * @param raum room
     * @return room
     */
    public Room getRoom(int raum) {
        return dungeon.getRoom(raum);
    }

    /**
     * Adds object to room
     *
     * @param raumID    room
     * @param equipment object
     * @return True, if successful
     */
    public boolean addObjectToRoom(int raumID, Equipment equipment) {
        return dungeon.addObjectToRoom(raumID, equipment);
    }

    /**
     * Deletes object from room
     *
     * @param raum room
     * @param name object-name
     * @return True, if successful
     */
    public boolean deleteObject(int raum, String name) {
        return dungeon.deleteObjectFromRoom(raum, name);
    }

    /**
     * Adds NPC to room
     *
     * @param raum room
     * @param npc  npc
     * @return True, if successful
     */
    public boolean addNPC(int raum, NPC npc) {
        return dungeon.addNPC(raum, npc);
    }

    /**
     * Removes NPC from room
     *
     * @param raum room
     * @param name npc-name
     * @return True, if successful
     */
    public boolean deleteNPC(int raum, String name) {
        return dungeon.deleteNPC(raum, name);
    }

    /**
     * Returns UserIDs of a room as List
     *
     * @param userID userID
     * @return List of IDs
     */
    public List<Integer> getPlayersAsList(int userID) {
        return dungeon.getPlayersAsList(userID);
    }

    /**
     * Returns UserIDs of a room as List
     *
     * @param raumID room
     * @return List of IDs
     */
    public List<Integer> getPlayersInRoom(int raumID) {
        return dungeon.getPlayersInRoom(raumID);
    }

    /**
     * Updates a Player-Object
     *
     * @param userID userID
     * @param player new Player-Object
     * @return True, if successful
     */
    public boolean playerUpdate(int userID, Player player) {
        if (playerIdRelation.containsKey(userID)) {
            playerIdRelation.replace(userID, player);
            return true;
        }
        return false;
    }

    public int getDungeonMaster() {
        return dungeonMaster;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isActive() {
        return active;
    }

    public Player getCharakter(int userID) {
        return playerIdRelation.get(userID);
    }

    public Dungeon getDungeon() {
        return dungeon;
    }

    public Map<Integer, Player> getPlayerIdRelation() {
        return playerIdRelation;
    }

    public List<Integer> getActivePlayers() {
        return activePlayers;
    }
}
