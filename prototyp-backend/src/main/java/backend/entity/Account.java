package backend.entity;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class Account {

    private final String name;
    private final String password;
    private final int id;

    public Account(String name, String password, int id) {
        this.name = name;
        this.id = id;
        this.password = password;
    }

    public Account(DBObject d) {
        this.name = (String) d.get("name");
        this.password = (String) d.get("passwort");
        this.id = (int) d.get("AccId");
    }

    public DBObject toDBObject() {
        return new BasicDBObject("name", this.name)
                .append("passwort", this.password)
                .append("AccId", this.id);
    }

    public String getPassword() {
        return password;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
