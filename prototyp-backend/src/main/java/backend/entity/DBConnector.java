package backend.entity;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import java.net.UnknownHostException;

public class DBConnector {
    public static MongoClientManager getConnection() {
        try {
            return new MongoClientManager(new MongoClient(new MongoClientURI("mongodb://193.196.54.12:27021")));
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return null;
        }
    }
}
