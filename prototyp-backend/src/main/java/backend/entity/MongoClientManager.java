package backend.entity;

import com.mongodb.MongoClient;

public class MongoClientManager implements AutoCloseable {

    private final MongoClient client;

    public MongoClientManager(MongoClient c) {
        client = c;
    }

    public MongoClient getClient() {
        return client;
    }

    @Override
    public void close() throws Exception {
        client.close();
    }
}
