package backend.work;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class GreetingController {
    @MessageMapping("/chat/fluestern/sender/{sender}/receiver/{receiver}")
    @SendTo("/topic/player-chat/{receiver}")
    public String fluestern(@DestinationVariable String sender, @DestinationVariable String receiver, String payload) {
        System.out.println("Log: Message sent from " + sender + " to " + receiver + "; Content: " + payload);
        return sender + " fluestert dir: " + payload;
    }

    @MessageMapping("/chat/schreien/sender/{sender}/receiver/{receiver}")
    @SendTo("/topic/player-chat/{receiver}")
    public String schreien(@DestinationVariable String sender, @DestinationVariable String receiver, String payload) {
        System.out.println("Log: Message sent from " + sender + " to " + receiver + "; Content: " + payload);
        return sender + " schreit: " + payload;
    }


    @MessageMapping("/chat/player-dm/sender/{sender}/receiver/{receiver}")
    @SendTo("/topic/dm-chat/{receiver}")
    public String player_to_dm(@DestinationVariable String sender, @DestinationVariable String receiver, String payload) {
        System.out.println("Log: Message sent from " + sender + " to Dugenon Master " + receiver + "; Content: " + payload);
        return sender + " sendet: " + payload;
    }

    @MessageMapping("/chat/dm-single/sender/{sender}/receiver/{receiver}")
    @SendTo("/topic/dm-chat/{receiver}")
    public String dm_single(@DestinationVariable String sender, @DestinationVariable String receiver, String payload) {
        System.out.println("Log: Message sent from " + sender + " to Dugenon Master " + receiver + "; Content: " + payload);
        return "Dungeonmaster an dich: " + payload;
    }

    @MessageMapping("/chat/dm-room/sender/{sender}/receiver/{receiver}")
    @SendTo("/topic/dm-chat/{receiver}")
    public String dm_room(@DestinationVariable String sender, @DestinationVariable String receiver, String payload) {
        System.out.println("Log: Message sent from " + sender + " to Dugenon Master " + receiver + "; Content: " + payload);
        return "Dungeonmaster an alle: " + payload;
    }


}
