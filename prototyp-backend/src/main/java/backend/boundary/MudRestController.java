package backend.boundary;

import backend.entity.Game;
import backend.entity.charaktergestaltung.CharacterOptions;
import backend.entity.charaktergestaltung.Equipment;
import backend.entity.charaktergestaltung.PlayerClass;
import backend.entity.charaktergestaltung.Race;
import backend.entity.raumgestaltung.NPC;
import backend.entity.raumgestaltung.Room;
import backend.entity.spieler.Character;
import backend.entity.spieler.Player;
import backend.services.AccountService;
import backend.services.DungeonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Controller fuer REST-Schnittstellen des MUDs
 */
@RestController
public class MudRestController {

    private final AccountService accountService;
    private final DungeonService dungeonService;

    @Autowired
    public MudRestController(AccountService accountService, DungeonService dungeonService) {
        this.accountService = accountService;
        this.dungeonService = dungeonService;
    }

    // Accounts

    /**
     * Registrates a new Account
     *
     * @param username Username
     * @param password Password
     * @return UserID
     */
    @PostMapping(value = "/api/accounts/neu/{username}/{password}")
    public int registrateAccount(@PathVariable String username, @PathVariable String password) {
        return accountService.registrate(username, password);
    }

    /**
     * Login
     *
     * @param username Username
     * @param passwort Password
     * @return UserID
     */
    @PutMapping(value = "/api/accounts/login/{username}/{passwort}")
    public int accountLogin(@PathVariable String username, @PathVariable String passwort) {
        return accountService.login(username, passwort);
    }


    // Übersichts

    /**
     * Returns all active Dungeons
     *
     * @return Map of DungeonIDs and Dungeonnames
     */
    @GetMapping(value = "/api/dungeon/allActive")
    public Map<Integer, String> getAllActiveDungeons() {
        return dungeonService.getActiveDungeons();
    }

    /**
     * Returns all inactive Dungeons
     *
     * @return Map of DungeonIDs and Dungeonnames
     */
    @GetMapping(value = "/api/dungeonMaster/inaktiveDungeons")
    public Map<Integer, String> getAllInactiveDungeons() {
        return dungeonService.getAllDungeons();
    }

    /**
     * Starts inactive dungeon
     *
     * @param userID    UserID (becomes DungeonMaster)
     * @param dungeonID DungeonID
     * @return true, if successful
     */
    @PostMapping(value = "/api/dungeonMaster/{userID}/start/{dungeonID}")
    public boolean startInactiveGame(@PathVariable int userID, @PathVariable int dungeonID) {
        return dungeonService.activateDungeon(dungeonID, userID);
    }

    /**
     * Creates new dungeon
     *
     * @param name Name of the Dungeons
     * @return DungeonID
     */
    @PostMapping(value = "/api/dungeonBuilder/{name}")
    public int createDungeon(@PathVariable String name) {
        return dungeonService.createDungeon(name);
    }

    /**
     * Checks, if a User already has a Player in a Dungeon
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     * @return true, if yes
     */
    @GetMapping(value = "/api/dungeon/{dungeonID}/{userID}")
    public boolean checkExistingPlayer(@PathVariable int dungeonID, @PathVariable int userID) {
        return dungeonService.checkExistingPlayers(dungeonID, userID);
    }


    //DungeonBuilder

    /**
     * Adds room to dungeon
     *
     * @param dungeonID DungeonID
     * @param room      room
     * @return roomID
     */
    @PostMapping(value = "/api/dungeonBuilder/{dungeonID}/raum")
    public Room addRoom(@PathVariable int dungeonID, @RequestBody Room room) {
        return dungeonService.addRoomToDungeon(dungeonID, room);
    }

    /**
     * Connects 2 rooms
     *
     * @param dungeonID DungeonID
     * @param raum1     Id of Room1
     * @param raum2     Id of Room 2
     */
    @PostMapping(value = "/api/dungeonBuilder/{dungeonID}/raumVerknuepfung/{raum1}/{raum2}")
    public boolean addRoomConnection(@PathVariable int dungeonID, @PathVariable int raum1, @PathVariable int raum2) {
        return dungeonService.connectRooms(dungeonID, raum1, raum2);
    }

    /**
     * Adds race to dungeon
     *
     * @param dungeonID DungeonID
     * @param race      new race
     */
    @PostMapping(value = "/api/dungeonBuilder/{dungeonID}/rasse")
    public boolean addRace(@PathVariable int dungeonID, @RequestBody Race race) {
        return dungeonService.addRaceToDungeon(dungeonID, race);
    }

    /**
     * Adds class to dungeon
     *
     * @param dungeonID    DungeonID
     * @param aPlayerClass new class
     */
    @PostMapping(value = "/api/dungeonBuilder/{dungeonID}/klasse")
    public boolean addClass(@PathVariable int dungeonID, @RequestBody PlayerClass aPlayerClass) {
        return dungeonService.addClassToDungeon(dungeonID, aPlayerClass);
    }

    /**
     * Adds equipment to dungeon
     *
     * @param dungeonID DungeonID
     * @param equipment new Eqipment
     */
    @PostMapping(value = "/api/dungeonBuilder/{dungeonID}/equipment")
    public boolean addEquipment(@PathVariable int dungeonID, @RequestBody Equipment equipment) {
        return dungeonService.addEquipmentToDungeon(dungeonID, equipment);
    }


    //Spiel beitreten

    /**
     * Returns Character-Options
     *
     * @param dungeonID DungeonID
     * @return Character-Options
     */
    @GetMapping(value = "/api/dungeon/{dungeonID}/charakterInfo")
    public CharacterOptions getCharakterOptions(@PathVariable int dungeonID) {
        return dungeonService.getCharakterOptions(dungeonID);
    }

    /**
     * Creates new Player in Dungeon
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     * @param character new Character
     */
    @PostMapping(value = "/api/dungeon/{dungeonID}/{userID}/{name}/charakter")
    public boolean createCharacter(@PathVariable int dungeonID, @PathVariable int userID, @PathVariable String name, @RequestBody Character character) {
        return dungeonService.addPlayerToDungeon(dungeonID, userID, name, character);
    }

    /**
     * Lets player join a game
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     * @return true, if successful
     */
    @PostMapping(value = "/api/dungeon/{dungeonID}/{userID}/betreten")
    public boolean joinGame(@PathVariable int dungeonID, @PathVariable int userID) {
        return dungeonService.joinDungeon(dungeonID, userID);
    }


    //Im Spiel

    /**
     * Returns Player information
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     * @return Player
     */
    @GetMapping(value = "/api/dungeon/{dungeonID}/{userID}/charakter")
    public Player getCharacter(@PathVariable int dungeonID, @PathVariable int userID) {
        return dungeonService.getCharakter(dungeonID, userID);
    }

    /**
     * Removes Player from dungeon
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     */
    @PostMapping(value = "/api/dungeon/{dungeonID}/{userID}/verlassen")
    public boolean exitGame(@PathVariable int dungeonID, @PathVariable int userID) {
        return dungeonService.exitDungeon(dungeonID, userID);
    }

    /**
     * Deletes Player from dungeon
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     */
    @PostMapping(value = "/api/dungeon/{dungeonID}/{userID}/abmelden")
    public boolean deleteCharacter(@PathVariable int dungeonID, @PathVariable int userID) {
        return dungeonService.deletePlayerFromDungeon(dungeonID, userID);
    }

    /**
     * Returns Room information of the room, the player is in
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     * @return Room-Information
     */
    @GetMapping(value = "/api/dungeon/{dungeonID}/{userID}/gebe-raum")
    public Room getRoom(@PathVariable int dungeonID, @PathVariable int userID) {
        return dungeonService.getRoom(dungeonID, userID);
    }

    /**
     * Returns UserID of Dungeonmaster
     *
     * @param dungeonID DungeonID
     * @return UserID
     */
    @GetMapping(value = "/api/dungeon/{dungeonID}/gebe-dungeonmaster")
    public int getDungeonMasterID(@PathVariable int dungeonID) {
        return dungeonService.getDungeonMaster(dungeonID);
    }

    /**
     * Returns UserIDs of the room the player is in
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     * @return List of UserIDs
     */
    @GetMapping(value = "/api/dungeon/{dungeonID}/{userID}/gebe-spieler-in-raum")
    public List<Integer> getPlayersInRoomByUser(@PathVariable int dungeonID, @PathVariable int userID) {
        return dungeonService.getPlayersAsList(dungeonID, userID);
    }


    //DungeonMaster

    /**
     * Closes Dungeon. Only possible, if no Player is in it.
     *
     * @param userID    UserID
     * @param dungeonID DungeonID
     * @return true, if successful
     */
    @PostMapping(value = "/api/dungeonMaster/{userID}/ende/{dungeonID}")
    public boolean stopGame(@PathVariable int userID, @PathVariable int dungeonID) {
        return dungeonService.deactivateDungeon(dungeonID, userID);
    }

    /**
     * Returns dungeon
     *
     * @param dungeonID DugneonID
     * @return Dungeon
     */
    @GetMapping(value = "api/dungeonMaster/dungeon/{dungeonID}")
    public Game getDungeon(@PathVariable int dungeonID) {
        return dungeonService.getDungeon(dungeonID);
    }

    /**
     * Puts player into another room. Only possible if rooms are connected.
     *
     * @param dungeonID DungeonID
     * @param userID    UserID
     * @param raum      new RoomID
     * @return true, if successful
     */
    @PostMapping(value = {"/api/dungeon/{dungeonID}/{userID}/raumWechsel/{raum}", "/api/dungeon/{dungeonID}/{userID}/raumWechselContr/{raum}"})
    public boolean switchRoom(@PathVariable int dungeonID, @PathVariable int userID, @PathVariable int raum) {
        return dungeonService.switchRoom(dungeonID, userID, raum);
    }

    /**
     * Interaction between Player and Object
     *
     * @param dungeonID  DungeonID
     * @param userID     UserID
     * @param objektName Name of the Object
     */
    @PutMapping(value = "/api/dungeon/{dungeonID}/{userID}/objekt/{objektName}")
    public boolean interactWithObject(@PathVariable int dungeonID, @PathVariable int userID, @PathVariable String objektName) {
        return dungeonService.interactWithObject(dungeonID, userID, objektName);
    }

    /**
     * Adds object to room
     *
     * @param dungeonID DungeonID
     * @param raum      RoomID
     * @param equipment Object
     * @return true, if successful
     */
    @PostMapping(value = "/api/dungeon/{dungeonID}/{raum}/objekt-hinzufuegen")
    public boolean addObject(@PathVariable int dungeonID, @PathVariable int raum, @RequestBody Equipment equipment) {
        return dungeonService.addObjectToRoom(dungeonID, raum, equipment);
    }

    /**
     * Deletes object from room
     *
     * @param dungeonID  DungeonID
     * @param raum       roomID
     * @param objektName name of object
     * @return true, if successful
     */
    @PostMapping(value = "/api/dungeon/{dungeonID}/{raum}/objekt-loeschen/{objektName}")
    public boolean deleteObject(@PathVariable int dungeonID, @PathVariable int raum, @PathVariable String objektName) {
        return dungeonService.deleteObjectFromRoom(dungeonID, raum, objektName);
    }

    /**
     * Adds NPC to room
     *
     * @param dungeonID DungeonID
     * @param raum      roomID
     * @param npc       NPC
     * @return true, if successful
     */
    @PostMapping(value = "/api/dungeon/{dungeonID}/{raum}/npc-hinzufuegen")
    public boolean addNPC(@PathVariable int dungeonID, @PathVariable int raum, @RequestBody NPC npc) {
        return dungeonService.addNPC(dungeonID, raum, npc);
    }

    /**
     * Deletes NPC from room
     *
     * @param dungeonID DungeonID
     * @param raum      roomID
     * @param npcName   name of NPC
     * @return true, if successful
     */
    @PostMapping(value = "/api/dungeon/{dungeonID}/{raum}/npc-loeschen/{npcName}")
    public boolean deleteNPC(@PathVariable int dungeonID, @PathVariable int raum, @PathVariable String npcName) {
        return dungeonService.deleteNPC(dungeonID, raum, npcName);
    }

    /**
     * Changes Player-Attributes
     *
     * @param dungeonID DungeonID
     * @param userID    userID
     * @param player    new Player-Object
     * @return true, if successful
     */
    @PostMapping(value = "/api/dungeon/{dungeonID}/{userID}/attribut-aendern")
    public boolean changePlayerAttributes(@PathVariable int dungeonID, @PathVariable int userID, @RequestBody Player player) {
        return dungeonService.playerUpdate(dungeonID, userID, player);
    }

    /**
     * returns UserIDs in room
     *
     * @param dungeonID DungeonID
     * @param raumID    roomID
     * @return List of UserIDs
     */
    @GetMapping(value = "/api/dungeon/{dungeonID}/{raumID}/gebeSpieler")
    public List<Integer> getPlayersInRoom(@PathVariable int dungeonID, @PathVariable int raumID) {
        return dungeonService.getPlayersInRoom(dungeonID, raumID);
    }
}
